import cv2
import numpy as np
import torchvision.models as models
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import numpy as np
import torchvision.transforms as transforms
from PIL import Image
import torch
from torchvision import datasets, transforms as T
import torchvision
import time
import copy
import os

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
eye_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_eye.xml")

squeezenet = models.squeezenet1_0(pretrained=True)
squeezenet.classifier[1] = nn.Conv2d(512, 2, kernel_size=(1,1), stride=(1,1))
net = squeezenet.cuda()
net.load_state_dict(torch.load('C:/Users/User/Desktop/Диплом/Сlassification/mask-classification/sq.pt'))

data_transforms = transforms.Compose([
    transforms.Resize(64, interpolation=2),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225])
])
faceCascade = face_cascade
eyeCascade = eye_cascade
video_capture = cv2.VideoCapture(0)
while True:
    # Capture frame-by-frame
    ret, frame = video_capture.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(gray,
                                         scaleFactor=1.1,
                                         minNeighbors=5,
                                         minSize=(60, 60),
                                         flags=cv2.CASCADE_SCALE_IMAGE)
    for (x,y,w,h) in faces:
        img =Image.fromarray(np.uint8(frame[y-20:y+h+40,x:x-20+w+40])).convert('RGB')
        img = data_transforms(img)
        torch.Tensor(img).unsqueeze(3).shape
        output = net(torch.Tensor(img).unsqueeze(0).cuda())
        xor, preds = torch.max(output.data, 1)

        if preds[0].item() == 0:
            color = (0,0,255)
        else:
            color = (0,255,0)
        cv2.rectangle(frame, (x, y), (x + w, y + h),color, 2)
        # Disply the resulting frame
    cv2.imshow('Video', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
video_capture.release()
cv2.destroyAllWindows()